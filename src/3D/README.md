# 3D

-	[Mixamo](https://www.mixamo.com);
-	[Car Low Poly](https://free3d.com/pt/3d-model/low-poly-car-40967.html);
-	[CarPorsche](https://free3d.com/pt/3d-model/car-tuned-34191.html);
-	[Lara Croft - Shorts Style](https://sketchfab.com/3d-models/lara-croft-shorts-style-8c08423bd7d44e7fabea8351431f5910);
-	[Lara Croft - Default Style](https://sketchfab.com/3d-models/lara-croft-default-style-2e729e157bab4d22aad095b568aaa264);

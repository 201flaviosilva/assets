# Assets

## Importar:
> import Assets from "../../Assets.js";

## Menu
- [3D](./3D/README.md);
- [Audio](./Audio/README.md);
- [BackGround](./Background/README.md);
- [Environment](./Environment/README.md);
- [Gifs](./Gifs/README.md);
- [Icons](./Icons/README.md);
- [Images](./Images/README.md);
- [Libs](./Libs/README.md);
- [Sprite](./Sprite/README.md);
- [Textures](./Textures/README.md);
- [Tile](./Tile/README.md);
- [UI](./UI/README.md);


- (Por favor, desculpe se uso algum asset e não tenha identificado o criador, caso tenha acontecido, por favor entre em contacto :))
- (Please, sorry if I use any asset and have not identified the creator, if it happened, please get in touch :))

# Icons

- [Apple](https://fontawesome.com/icons/apple?style=brands);
- [Code](https://feathericons.com/?query=code);
- [Linux](https://fontawesome.com/icons/linux?style=brands);
- [Web](https://fontawesome.com/icons/globe?style=solid);
- [Windows](https://fontawesome.com/icons/windows?style=brands);

[Back](../Assets.md);
